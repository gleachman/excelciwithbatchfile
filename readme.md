Continous Integration on a spreadsheet
--------------------------------------



**Objectives for this repository**

>This project is intended to illustrate use of a batch file for CI. It provides a lightweight system for doing Continuous Integration on a spreadsheet. 

>It has also been tested against SQLServer codebase, and by using NUnit-Console can be extended to any codebase.

**System Under Test**

>In this example I am using the [Bridge Dealing Book]

**Framework**

>Continuous integration has many moving parts. For this example I am using the following components. 

> - A batch file that runs in the CI environment against the working copy folder.
> - The [vba unit testing framework workbook] [1] described [here] [2].
> - [git commandline toolset] [git tutorial]
> - reporting results using an [email client] [email reporting]

**Tips on CI**

> Some tips from [atlassian] [atlassian tips] on doing CI with git.

**TODO:**

 - *how to : setting up the email client*
 - *zip file download of the full example*
 - *youtube clip of the full example*
 - *example output*

  [email reporting]: http://sourceforge.net/projects/blat
  [1]: https://bitbucket.org/gleachman/vba-unittest-framework/downloads/unittestFramework.xla
  [2]: https://bitbucket.org/gleachman/vba-unittest-framework
  [git tutorial]: https://www.atlassian.com/git/
  [atlassian tips]: https://www.atlassian.com/git/articles/ci-friendly-git-repos
  [source code]: http://www.attacklab.net/showdown-v0.9.zip
  [blank page]: ?blank=1 "Clear all text"
  [Bridge Dealing Book]: https://bitbucket.org/gleachman/vba-unittest-framework/downloads/BridgeDealsTests.xls
